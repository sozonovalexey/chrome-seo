window.addEventListener("load", function() {

    var $container = $('#search-form');
    var $el = $('#keywords_text');


    if ($el.length) {

        $container.css({'position': 'relative'});
        $container.after('<a href="javascript:;" class="js-seo-copy" style="position: absolute;bottom: 30px;right:20px;padding: 5px 10px;border:1px solid #000;background: #fff;">Копировать</a>');

        var $target = $el;



        $('body').on('click touchstart', '.js-seo-copy', function (e) {
            e.preventDefault();


            var html = $target.html() + '';

            //var text = html.replace(/<span (.*)>(.*?)<\/span>/gi, '$2,');
            var text = html.replace(/<span(.*?)>/gi, '');
            text = text.replace(/<\/span>/gi, ',');
            text = text.trim();
            text = text.substr(0, text.length - 1);

            var dummy = document.createElement('textarea');
            document.body.appendChild(dummy);
            dummy.setAttribute('id', 'dummy_id');
            document.getElementById('dummy_id').value = text;
            dummy.select();
            document.execCommand('copy');
            document.body.removeChild(dummy);

        });
    }


}, false);
